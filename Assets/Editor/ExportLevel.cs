///////////////////////////////////////////////////////////////////////////////////////////////
// Unity Level Exporter configured for Ninja game - by Johan Rensenbrink
// Built on terrain OBJ exporter
// Also fixed the retarded indentation
// 
// CURRENTLY SUPPORTED OBJECTS:
// Terrain - Any number of terrain objects
// Meshes - Any number of meshes
// Sub-meshes
// Shared materials & Textures
// Directional Lights
// Point Lights
//
// Terrain Exporter Credits
// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// C # manual conversion work by Yun Kyu Choi
///////////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Diagnostics;

enum ProgressType {typeTexture=0, typeTerrain, typeModel}
enum SaveResolution {Highest=0, High, Medium, Low, Lowest}

struct MaterialStruct {
	public string name;
	public bool lighting;
	public float[] ambient;
	public float[] diffuse;
	public float[] specular;
}

class ExportLevel : EditorWindow
{
	static SaveResolution saveResolution = SaveResolution.High;
	
	// Statics everywhere (blame Unity)
   	static string filePath;
	static string levelName = "";
   	static bool success = true;
	static int textureCount = 0;
	static int textureTotal = 0;
	static GUIStyle warningStyle = new GUIStyle();
	
   	// Make sure designers dont try to export single-instance objects.
   	// (Because they will)
	static bool player = false;
	
   	// Terrain exporting stuff
   	static int tCount;
   	static int counter;
   	static int totalCount;
   	static int progressUpdateInterval = 10000;
   	static int terrainCount = 0;
   	static int terrainTotal = 0;
	static int modelCount = 0;
	static int modelTotal = 0;
   	static TerrainData terrain;
   	static Vector3 terrainPos;
	static string saveDirectory = "";
	
   	[MenuItem("Shuriken/Export Ninja Level...", false, 13)]
   	static void Init() {
      	// Reset flags
	  	success = true;
      	terrain = null;
		player = false;
		tCount = 0;
		counter = 0;
		totalCount = 0;
		terrainCount = 0;
		terrainTotal = 0;
		textureCount = 0;
		textureTotal = 0;
		modelCount = 0;
		modelTotal = 0;
		
		ExportLevel export = EditorWindow.GetWindowWithRect<ExportLevel>(new Rect(0, 0, 300, 110));
		export.Show();
	}
	
	void OnGUI() {
		GUILayout.Label("Select a terrain quality to export.");
      	saveResolution = (SaveResolution) EditorGUILayout.EnumPopup("Terrain Quality", saveResolution);
		
		warningStyle.normal.textColor = Color.red;
		if (saveResolution == SaveResolution.Highest)
			GUILayout.Label("Warning: Large file size expected for terrain!", warningStyle);
		else 
			GUILayout.Label(" ");
		GUILayout.Label(" ");
		
      	if (GUILayout.Button("Export")) {
			Export();
			EditorWindow.GetWindow<ExportLevel>().Close();
      	}
		
		if (GUILayout.Button("Cancel")) {
        	EditorWindow.GetWindow<ExportLevel>().Close();
			return;
     	}
   	}
	
	static void Export() {	
		// Save dialogue to select a folder
	  	filePath = EditorUtility.SaveFolderPanel("Export Ninja Level", saveDirectory, "");
		saveDirectory = filePath;
		
	  	// Find how many terrain objects we need to export to give a good progress indication
	  	GameObject[] objects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];
      	foreach(GameObject gameObj in objects) {
			// If we find terrain, export it with the terrain exporter thingy
		    if(gameObj.tag == "Terrain") {
				++terrainTotal;
				++textureTotal;
		    }
			
			// If we find a model, export it with the model exporter thingy
			if(gameObj.tag == "Cube") {
				++modelTotal;
				++textureTotal;
			}

			if(gameObj.tag == "Player") {		
				if (player) {
					EditorUtility.DisplayDialog("Export Ninja Level", "You can only have one player start, dumbass!", "Derp");
					return;
				} else {
					player = true;
					Player playerObj = gameObj.GetComponent<Player>();
					levelName = playerObj.levelName;
				}
			}
		}
		
		// Check if we have required components.
		if (levelName == "") {
			EditorUtility.DisplayDialog("Export Ninja Level", "You forgot to set a level name.", "Oops!");
			return;
		}
		
		if (!player) {
			EditorUtility.DisplayDialog("Export Ninja Level", "You forgot to place a Player Start.", "Oops!");
			return;
		}

		// Load up a progress indicator
		EditorUtility.DisplayProgressBar("Setting Up Directory Structure", "", 0.0f);
		
		// Replace any previous content. Stops unused terrain files from forming.
		if (System.IO.Directory.Exists(filePath + "/" + levelName + "/"))
			System.IO.Directory.Delete(filePath + "/" + levelName + "/",true);
		
		// (Re)create the directory
		System.IO.Directory.CreateDirectory(filePath + "/" + levelName + "/");
		
		int nextNodeID = 0;
		foreach(GameObject gameObj in objects) {
			if(gameObj.tag == "PathNode") {
			PathNode pathNode = gameObj.GetComponent<PathNode>();
				pathNode.ID = nextNodeID++;
			}
		}
		
		// Export level file
      	StreamWriter sw = new StreamWriter(filePath + "/" + levelName + ".level");
		try {	
			// Write a default header
			sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sw.WriteLine("<!-- XML file generated by Johan Rensenbrink's Unity Level Exporter script. -->");
			sw.WriteLine("");
			
			// Initialise the level
			sw.WriteLine("<level>");
			
			// Objects are sorted in an order to allow for a clean consistent file format and loading progress.
		  	// Save terrain
			sw.WriteLine("\t<!-- Terrain Data -->");
	      	foreach(GameObject gameObj in objects) {
				// If we find terrain, export it with the terrain exporter thingy
			    if(gameObj.tag == "Terrain") {
					++terrainCount;
					terrain = gameObj.GetComponent<Terrain>().terrainData;
	           		terrainPos = gameObj.transform.position;
					
					sw.WriteLine("\t<terrain iter=\"" + terrainCount + "\">");
					sw.WriteLine("\t\t<position X=\"" + terrainPos.x + "\" Y=\"" + terrainPos.y + "\" Z=\"" + terrainPos.z + "\"/>");
					for (int i = 0; i < terrain.splatPrototypes.Length; ++i) {
						//sw.WriteLine("\t\t<texture xOffset=\"" + terrain.splatPrototypes[i].tileOffset.x + "\" yOffset=\"" + terrain.splatPrototypes[0].tileOffset.y + "\" xTile=\"" + terrain.splatPrototypes[0].tileSize.x + "\" yTile=\"" + terrain.splatPrototypes[0].tileSize.y + "\"/>");
						
						// Convert the terrain splat map to a PNG and save it.
						ExportTexture(terrain.splatPrototypes[0].texture, filePath + "/" + levelName + "/terrain" + terrainCount + "_splat" + i + ".png");
						MaterialStruct mat = MaterialInit();
						mat.name = "terrain" + terrainCount + "_splat" + i;
						mat.specular[0] = 0.05f;
						mat.specular[1] = 0.05f;
						mat.specular[2] = 0.05f;
						ExportMaterial(mat, mat.name);
					}
					sw.WriteLine("\t</terrain>");
					
					// Export the terrain to an OBJ model
					ExportTerrain();
			    }
			}
			sw.WriteLine("\t");
			
			// Write the player start.
			sw.WriteLine("\t<!-- Player Data -->");
			foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "Player") {
					sw.WriteLine("\t<player>");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t</player>");
				}
			}
			sw.WriteLine("\t");
			
			// Write enemy data.
			sw.WriteLine("\t<!-- Guard Data -->");
			foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "Guard") {
					Guard guard = gameObj.GetComponent<Guard>();
					sw.WriteLine("\t<guard startNode=\"" + guard.startNode.ID + "\">");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t</guard>");
				}
			}
			sw.WriteLine("\t");
			
			// Write path data.
			sw.WriteLine("\t<!-- Path Data -->");
			foreach(GameObject gameObj in objects) {
				if(gameObj.tag == "PathNode") {
				PathNode pathNode = gameObj.GetComponent<PathNode>();
					sw.WriteLine("\t<path ID=\"" + pathNode.ID + "\">");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					if (pathNode.next)
						sw.WriteLine("\t\t<next ID=\"" + pathNode.next.ID + "\"/>");
					if (pathNode.previous)
						sw.WriteLine("\t\t<previous ID=\"" + pathNode.previous.ID + "\"/>");
					sw.WriteLine("\t</path>");
				}
			}
			sw.WriteLine("\t");
			
			// Write particle data.
			sw.WriteLine("\t<!-- Particle Data -->");
			foreach(GameObject gameObj in objects) {
				if(gameObj.tag == "Fire") {
				Fire fire = gameObj.GetComponent<Fire>();
					sw.WriteLine("\t<fire rate=\"" + fire.emissionRate + "\" life=\"" + fire.lifeTime + "\" size=\"" + fire.fireSize + "\" height\"" + fire.fireHeight + "\">");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t</fire>");
				}
			}
			sw.WriteLine("\t");
			
			// Write Unity geometry data.
			sw.WriteLine("\t<!-- Geometry Data -->");
			foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "Cube") {
					++modelCount;
					
					// Determine whether this object has submeshes or is a single mesh
					MeshFilter mesh = gameObj.GetComponent<MeshFilter>();
					Material texture = gameObj.GetComponent<MeshRenderer>().sharedMaterial;
					
					sw.WriteLine("\t<geometry name=\"" + gameObj.name + "\">");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t\t<direction W=\"" + gameObj.transform.rotation.w + "\" X=\"" + gameObj.transform.rotation.x + "\" Y=\"" + gameObj.transform.rotation.y + "\" Z=\"" + gameObj.transform.rotation.z + "\"/>");
					sw.WriteLine("\t\t<scale X=\"" + gameObj.transform.localScale.x + "\" Y=\"" + gameObj.transform.localScale.y + "\" Z=\"" + gameObj.transform.localScale.z + "\"/>");
					sw.WriteLine("\t</geometry>");
					
					sw.WriteLine("test 0");
					
					// Export the textures & materials
					if (texture.mainTexture)
						ExportTexture(texture.mainTexture as Texture2D, filePath + "/" + levelName + "/" + texture.mainTexture.name + ".png");
					
					sw.WriteLine("test 1");
					
					// Sort out material variables
					MaterialStruct mat = MaterialInit();
					mat.name = gameObj.name;
					if (gameObj.GetComponent<MaterialScript>()) {
						MaterialScript matScript = gameObj.GetComponent<MaterialScript>();
						mat.ambient[0] = matScript.ambient.R;
						mat.ambient[1] = matScript.ambient.G;
						mat.ambient[2] = matScript.ambient.B;
						mat.diffuse[0] = matScript.diffuse.R;
						mat.diffuse[1] = matScript.diffuse.G;
						mat.diffuse[2] = matScript.diffuse.B;
						mat.specular[0] = matScript.specularity.R;
						mat.specular[1] = matScript.specularity.G;
						mat.specular[2] = matScript.specularity.B;
						mat.specular[3] = matScript.specularity.A;
						mat.specular[4] = matScript.specularity.Shinyness;
						mat.lighting = matScript.lighting;
					}
					
					if (texture.mainTexture)
						ExportMaterial(mat, texture.mainTexture.name);
					
					sw.WriteLine("test 2");
					
					string materialName = mat.name;

					// Export the model to an MDL
					ExportModel(mesh.sharedMesh, gameObj.name, materialName);
					
					sw.WriteLine("test 3");
			    }
			}
			sw.WriteLine("\t");		
			
			sw.WriteLine("\t<!-- Model Data -->");
			// If we find a model, export it with the model exporter thingy
			foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "Model") {				
					sw.WriteLine("\t<model name=\"" + gameObj.name + "\">");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t\t<direction W=\"" + gameObj.transform.rotation.w + "\" X=\"" + gameObj.transform.rotation.x + "\" Y=\"" + gameObj.transform.rotation.y + "\" Z=\"" + gameObj.transform.rotation.z + "\"/>");
					sw.WriteLine("\t\t<scale X=\"" + gameObj.transform.localScale.x + "\" Y=\"" + gameObj.transform.localScale.y + "\" Z=\"" + gameObj.transform.localScale.z + "\"/>");
					sw.WriteLine("\t</model>");
			    }
			}
			sw.WriteLine("\t");
			
		  	// Save lighting information
			sw.WriteLine("\t<!-- Environment Light Data -->");
	      	foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "sunlight") {
					sw.WriteLine("\t<directionalLight>");
					sw.WriteLine("\t\t<direction W=\"" + gameObj.transform.rotation.w + "\" X=\"" + gameObj.transform.rotation.x + "\" Y=\"" + gameObj.transform.rotation.y + "\" Z=\"" + gameObj.transform.rotation.z + "\"/>");
					sw.WriteLine("\t\t<colour R=\"" + gameObj.GetComponent<Light>().color.r + "\" G=\"" + gameObj.GetComponent<Light>().color.g + "\" B=\"" + gameObj.GetComponent<Light>().color.b + "\"/>");
					sw.WriteLine("\t</directionalLight>");
			    }
			}
			sw.WriteLine("\t");
			
		  	// Save lighting information
			sw.WriteLine("\t<!-- Point Light Data -->");
	      	foreach(GameObject gameObj in objects) {
			    if(gameObj.tag == "Light") {
					sw.WriteLine("\t<pointLight>");
					sw.WriteLine("\t\t<position X=\"" + gameObj.transform.position.x + "\" Y=\"" + gameObj.transform.position.y + "\" Z=\"" + gameObj.transform.position.z + "\"/>");
					sw.WriteLine("\t\t<colour R=\"" + gameObj.GetComponent<Light>().color.r + "\" G=\"" + gameObj.GetComponent<Light>().color.g + "\" B=\"" + gameObj.GetComponent<Light>().color.b + "\"/>");
					sw.WriteLine("\t\t<range Value=\"" + gameObj.GetComponent<Light>().range + "\"/>");
					sw.WriteLine("\t</pointLight>");
			    }
			}
			sw.WriteLine("\t");
			
			// Save fog info, if fog is enabled
			if (RenderSettings.fog) {
				sw.WriteLine("\t");
				sw.WriteLine("\t<!-- Fog Data -->");
				sw.WriteLine("\t<fog start=\"" + RenderSettings.fogStartDistance + "\" end=\"" + RenderSettings.fogEndDistance + "\">");
				sw.WriteLine("\t\t<colour R=\"" + RenderSettings.fogColor.r + "\" G=\"" + RenderSettings.fogColor.g + "\" B=\"" + RenderSettings.fogColor.b + "\"/>");
				sw.WriteLine("\t</fog>");
			}
			
			// Close the root element
			sw.WriteLine("</level>");
		} catch(Exception err) {
			EditorUtility.ClearProgressBar();
			success = false;
       		UnityEngine.Debug.Log("Error saving file: " + err.Message);
      	}
      	sw.Close();
		
		// Clear the progress bar.
		EditorUtility.ClearProgressBar();
		
		// Tell designer what happened.
		if (success) {
			// Run a batch script that automatically converts the XML files to MESH files, then deletes the XML files
			// Only gets run if its in the right directory - otherwise it will have to be executed.
			
			if (System.IO.File.Exists(filePath + "\\meshConverter.bat")) {
				var XMLToMesh = new Process(); 
				XMLToMesh.StartInfo.FileName = filePath + "\\meshConverter.bat";
				
				// Switch the slashes and get OgreXMLConverter directory.
				string tempPath = filePath.Replace("/","\\");
				string exePath = tempPath.Substring(0,tempPath.Length-13);
				XMLToMesh.StartInfo.Arguments = "\"" + tempPath + "\\" + levelName + "\" \"" + exePath + "\""; 
				XMLToMesh.Start();
				
				EditorUtility.DisplayDialog("Export Ninja Level", "Exporting done!", "Yay!");
			} else {
				EditorUtility.DisplayDialog("Export Ninja Level", "Exporting done - Converter not detected. If you wish to run the game you will have to convert the files first, or just give it to a programmer ;)!", "Okay!");
			}
		} else {
			EditorUtility.DisplayDialog("Export Ninja Level", "Exporting failed! Something went wrong :(.", "Aw..");
		}
	}
	
	static MaterialStruct MaterialInit() {
		MaterialStruct mat = new MaterialStruct();
		mat.ambient = new float[3]{1.0f,1.0f,1.0f};
		mat.diffuse = new float[3]{1.0f,1.0f,1.0f};
		mat.specular = new float[5]{0.2f,0.2f,0.2f,1.0f,25.0f};
		mat.lighting = true;
		return mat;
	}
	
	static void ExportMesh(string savePath, Vector3[] tVertices, Vector2[] tUV, Vector3[] tNormals, int[] tPolys, string materialName, bool terrain) {
		//Export to file
		StreamWriter sw = new StreamWriter(savePath);
		try {
         	// Write vertices
         	System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
         	totalCount += (tVertices.Length * 2 + tPolys.Length / 3) / progressUpdateInterval;
			
			// Start of XML header
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("<mesh>");
			sb.AppendLine("\t<submeshes>");
			
			if (materialName != "")
				sb.Append("\t\t<submesh material=\"").Append(materialName).Append("\" usesharedvertices=\"false\" use32bitindexes=\"true\" operationtype=\"triangle_list\">").AppendLine();
			else
				sb.Append("\t\t<submesh material=\"\" usesharedvertices=\"false\" use32bitindexes=\"true\" operationtype=\"triangle_list\">").AppendLine();

			sb.Append("\t\t\t<faces count=\"").Append((tPolys.Length/3).ToString()).Append("\">").AppendLine();
			
			// Write indices
			for (int i = 0; i < tPolys.Length; i += 3) {
				if (terrain) {UpdateProgress(ProgressType.typeTerrain);}else{UpdateProgress(ProgressType.typeModel);}
				sb.Append("\t\t\t\t<face v1=\"").Append(tPolys[i].ToString()).Append("\" v2=\"").Append(tPolys[i+2].ToString()).Append("\" v3=\"").Append(tPolys[i+1].ToString()).Append("\" />").AppendLine();
			}
			
			// Write vertices
			sb.AppendLine("\t\t\t</faces>");
			sb.Append("\t\t\t<geometry vertexcount=\"").Append(tVertices.Length.ToString()).Append("\">").AppendLine();
			sb.AppendLine("\t\t\t\t<vertexbuffer positions=\"true\" normals=\"true\">");
			
			for (int i = 0; i < tVertices.Length; i++) {
				if (terrain) {UpdateProgress(ProgressType.typeTerrain);}else{UpdateProgress(ProgressType.typeModel);}
				sb.AppendLine("\t\t\t\t\t<vertex>");
				sb.Append("\t\t\t\t\t\t<position x=\"").Append(tVertices[i].x.ToString()).Append("\" y=\"").Append(tVertices[i].y.ToString()).Append("\" z=\"").Append((-tVertices[i].z).ToString()).Append("\" />").AppendLine();
				sb.Append("\t\t\t\t\t\t<normal x=\"").Append(tNormals[i].x.ToString()).Append("\" y=\"").Append(tNormals[i].y.ToString()).Append("\" z=\"").Append((-tNormals[i].z).ToString()).Append("\" />").AppendLine();
				sb.AppendLine("\t\t\t\t\t</vertex>");
			}
			
			// Write UVs
			sb.AppendLine("\t\t\t\t</vertexbuffer>");
			sb.AppendLine("\t\t\t\t<vertexbuffer texture_coord_dimensions_0=\"float2\" texture_coords=\"1\">");
			for (int i = 0; i < tUV.Length; i++) {
				if (terrain) {UpdateProgress(ProgressType.typeTerrain);}else{UpdateProgress(ProgressType.typeModel);}
				sb.AppendLine("\t\t\t\t\t<vertex>");
				sb.Append("\t\t\t\t\t\t<texcoord u=\"").Append(tUV[i].x.ToString()).Append("\" v=\"").Append((-tUV[i].y).ToString()).Append("\" />").AppendLine();
				sb.AppendLine("\t\t\t\t\t</vertex>");
			}
			sb.AppendLine("\t\t\t\t</vertexbuffer>");
			
			// XML footer
			sb.AppendLine("\t\t\t</geometry>");
			sb.AppendLine("\t\t</submesh>");
			sb.AppendLine("\t</submeshes>");
			sb.AppendLine("</mesh>");
			
			sw.WriteLine(sb);
      	}
     	catch(Exception err) {
			EditorUtility.ClearProgressBar();
			success = false;
       		UnityEngine.Debug.Log("Error saving file: " + err.Message);
      	}
      	sw.Close();
	}
	
	// Export a single mesh model
	static void ExportModel(Mesh a_mesh, string a_name, string a_materialName) {
		string path = filePath + "/" + levelName + "/" + a_name + ".mesh.xml";
		
		// Don't export meshes that already exist.
		if (!System.IO.File.Exists(path)) {	
			Vector3[] tVertices = a_mesh.vertices;
			Vector2[] tUV = a_mesh.uv;
			Vector3[] tNormals = a_mesh.normals;
			
			//Export to file
			StreamWriter sw = new StreamWriter(path);
			try {
		     	// Write mesh
		     	System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
				
				// Start of XML header
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("<mesh>");
				
				// Write vertices
				sb.Append("\t<sharedgeometry vertexcount=\"").Append(tVertices.Length.ToString()).Append("\">").AppendLine();
				sb.AppendLine("\t\t<vertexbuffer positions=\"true\" normals=\"true\" texture_coord_dimensions_0=\"float2\" texture_coords=\"1\">");
				
				for (int i = 0; i < tVertices.Length; i++) {
					if (terrain) {UpdateProgress(ProgressType.typeTerrain);}else{UpdateProgress(ProgressType.typeModel);}
					sb.AppendLine("\t\t\t<vertex>");
					sb.Append("\t\t\t\t<position x=\"").Append(tVertices[i].x.ToString()).Append("\" y=\"").Append(tVertices[i].y.ToString()).Append("\" z=\"").Append((-tVertices[i].z).ToString()).Append("\" />").AppendLine();
					sb.Append("\t\t\t\t<normal x=\"").Append(tNormals[i].x.ToString()).Append("\" y=\"").Append(tNormals[i].y.ToString()).Append("\" z=\"").Append((-tNormals[i].z).ToString()).Append("\" />").AppendLine();
					sb.Append("\t\t\t\t<texcoord u=\"").Append(tUV[i].x.ToString()).Append("\" v=\"").Append((-tUV[i].y).ToString()).Append("\" />").AppendLine();
					sb.AppendLine("\t\t\t</vertex>");
				}
				sb.AppendLine("\t\t</vertexbuffer>");
				sb.AppendLine("\t</sharedgeometry>");
				
				sb.AppendLine("\t<submeshes>");
				
				// Loop through submeshes
				for (int j=0;j<a_mesh.subMeshCount;++j) {
					int[] tPolys = a_mesh.GetTriangles(j);//triangles;
					totalCount += (tVertices.Length * 2 + tPolys.Length / 3) / progressUpdateInterval;
					
					if (a_materialName != "")
						sb.Append("\t\t<submesh material=\"").Append(a_materialName).Append("\" usesharedvertices=\"true\" use32bitindexes=\"true\" operationtype=\"triangle_list\">").AppendLine();
					else
						sb.Append("\t\t<submesh material=\"\" usesharedvertices=\"true\" use32bitindexes=\"true\" operationtype=\"triangle_list\">").AppendLine();
					
					sb.Append("\t\t\t<faces count=\"").Append((tPolys.Length/3).ToString()).Append("\">").AppendLine();
					
					// Write indices
					for (int i = 0; i < tPolys.Length; i += 3) {
						if (terrain) {UpdateProgress(ProgressType.typeTerrain);}else{UpdateProgress(ProgressType.typeModel);}
						sb.Append("\t\t\t\t<face v1=\"").Append(tPolys[i].ToString()).Append("\" v2=\"").Append(tPolys[i+2].ToString()).Append("\" v3=\"").Append(tPolys[i+1].ToString()).Append("\" />").AppendLine();
					}
					sb.AppendLine("\t\t\t</faces>");
					
					// XML footer
					sb.AppendLine("\t\t</submesh>");
				}
				sb.AppendLine("\t</submeshes>");			
				sb.AppendLine("</mesh>");
				
				sw.WriteLine(sb);
		  	}
		 	catch(Exception err) {
				EditorUtility.ClearProgressBar();
				success = false;
		   		UnityEngine.Debug.Log("Error saving file: " + err.Message);
		  	}
		  	sw.Close();
		}
	}
	
	// Export terrain
  	static void ExportTerrain() {
      	int w = terrain.heightmapWidth;
        int h = terrain.heightmapHeight;
      	Vector3 meshScale = terrain.size;
      	int tRes = (int)Mathf.Pow(2, (int)saveResolution );
      	meshScale = new Vector3(meshScale.x / (w - 1) * tRes, meshScale.y, meshScale.z / (h - 1) * tRes);
      	//Vector2 uvScale = new Vector2(1.0f / (w - 1), 1.0f / (h - 1));
      	float[,] tData = terrain.GetHeights(0, 0, (int)w, (int)h);

      	w = (w - 1) / tRes + 1;
      	h = (h - 1) / tRes + 1;
      	Vector3[] tVertices = new Vector3[w * h];
      	Vector2[] tUV = new Vector2[w * h];
		Vector3[] tNormals = new Vector3[w * h];
      	int[] tPolys;

      	tPolys = new int[(w - 1) * (h - 1) * 6];
		
      	// Build vertices, UVs and Normals
      	for (float y = 0; y < h; y++) {
         	for (float x = 0; x < w; x++) {
            	tVertices[(int)y * (int)w + (int)x] = Vector3.Scale(meshScale, new Vector3((int)y, tData[(int)x * (int)tRes, (int)y * (int)tRes], (int)x)) + terrainPos;
				tUV[(int)y * (int)w + (int)x] = new Vector2((x * tRes) / (terrain.splatPrototypes[0].tileSize.x), (y * tRes) / (terrain.splatPrototypes[0].tileSize.y));
				Vector3 worldPos = Vector3.Scale(meshScale, new Vector3((int)y, tData[(int)x * (int)tRes, (int)y * (int)tRes], (int)x)) + terrainPos;
				Vector3 terrainLocalPos = worldPos - terrainPos;
  				Vector3 normalizedPos = new Vector2(
					Mathf.InverseLerp(0.0f, terrain.size.x, terrainLocalPos.x),
                 	Mathf.InverseLerp(0.0f, terrain.size.z, terrainLocalPos.z));
    			tNormals[(int)y * (int)w + (int)x] = terrain.GetInterpolatedNormal(normalizedPos.x, normalizedPos.y);
         	}
      	}

      	int index = 0;
     	// Build triangle indices: 3 indices into vertex array for each triangle
     	for (int y = 0; y < h - 1; ++y) {
        	for (int x = 0; x < w - 1; ++x) {
           		// For each grid cell output two triangles
           		tPolys[index] = ((int)y * (int)w) + (int)x;
           		tPolys[index+2] = (((int)y + 1) * (int)w) + (int)x;
           		tPolys[index+1] = ((int)y * (int)w) + (int)x + 1;

           		tPolys[index+3] = (((int)y + 1) * (int)w) + (int)x;
           		tPolys[index+5] = (((int)y + 1) * (int)w) + (int)x + 1;
           		tPolys[index+4] = ((int)y * (int)w) + x + 1;
     			
				index += 6;
			}
     	}

		string path = filePath + "/" + levelName + "/terrain" + terrainCount + ".mesh.xml";
		string splatName = "terrain" + terrainCount + "_splat0";
		ExportMesh (path, tVertices, tUV, tNormals, tPolys, splatName, true);
			
      	terrain = null;
   	}
	
	// With many or large textures this gives an indication what's happening.
	static void UpdateProgress(ProgressType type) {
		switch (type) {
			case ProgressType.typeTexture:
				EditorUtility.DisplayProgressBar("Converting & Exporting Textures (" + textureCount + " of " + textureTotal + ")", "", (float)textureCount/(float)textureTotal);
  				break;
			case ProgressType.typeModel:
				if (counter++ == progressUpdateInterval) {
        			counter = 0;
					EditorUtility.DisplayProgressBar("Exporting Model (" + modelCount + " of " + modelTotal + ")", "", Mathf.InverseLerp(0, totalCount, ++tCount));
   				}
				break;
			case ProgressType.typeTerrain:
				if (counter++ == progressUpdateInterval) {
        			counter = 0;
					EditorUtility.DisplayProgressBar("Exporting Terrain (" + terrainCount + " of " + terrainTotal + ")", "", Mathf.InverseLerp(0, totalCount, ++tCount));
   				}
				break;
			default:
				EditorUtility.DisplayProgressBar("Exporting...", "", 0.5f);
				break;
		}
	}
	
	// Converts and exports the texture.
	static void ExportTexture(Texture2D texture, string fileName) {
		if (!System.IO.File.Exists(fileName)) {	
			++textureCount;
			UpdateProgress(ProgressType.typeTexture);
			byte[] textureData = ConvertToPNG(AssetDatabase.GetAssetPath(texture));
			UpdateProgress(ProgressType.typeTexture);
			if (BitConverter.ToUInt32(textureData, 0) != 0) System.IO.File.WriteAllBytes(fileName, textureData);
		}
	}
	
	// Export the material file that Ogre actually uses to read texture data.
	static void ExportMaterial(MaterialStruct mat, string textureName) {
		string materialPath = filePath + "/" + levelName + "/" + mat.name + ".material";
			if (!System.IO.File.Exists(materialPath)) {		
			StreamWriter sw = new StreamWriter(materialPath);
			try {
				StringBuilder sb = new StringBuilder();
				sb.Append("material ").Append(mat.name).AppendLine();
				sb.AppendLine("{");
				sb.AppendLine("\ttechnique");
				sb.AppendLine("\t{");
				sb.AppendLine("\t\tpass");
				sb.AppendLine("\t\t{");
				if (!mat.lighting) {
					sb.AppendLine("\t\t\tlighting off");
				} else {
					sb.AppendLine("\t\t\tlighting on");
				}
				sb.AppendLine("\t\t\tambient " + mat.ambient[0] + " " + mat.ambient[1] + " " + mat.ambient[2]);
				sb.AppendLine("\t\t\tdiffuse " + mat.diffuse[0] + " " + mat.diffuse[1] + " " + mat.diffuse[2]);
				sb.AppendLine("\t\t\tspecular " + mat.specular[0] + " " + mat.specular[1] + " " + mat.specular[2] + " " + mat.specular[3] + " " + mat.specular[4]);
				sb.AppendLine();
				sb.AppendLine("\t\t\ttexture_unit");
				sb.AppendLine("\t\t\t{");
				sb.Append("\t\t\t\ttexture ").Append(textureName).Append(".png").AppendLine();
				sb.AppendLine("\t\t\t}");
				sb.AppendLine("\t\t}");
				sb.AppendLine("\t}");
				sb.AppendLine("}");
				sw.Write(sb);
			}
			catch(Exception err) {
				success = false;
	       		UnityEngine.Debug.Log("Error saving file: " + err.Message);
	      	}
	      	sw.Close();
		}
	}
		
	// Convert the texture format to ARGB32, encode it to a PNG, and switch it back to compressed, yay!
	static byte[] ConvertToPNG(string path) {
		TextureImporter tImporter = AssetImporter.GetAtPath( path ) as TextureImporter;
        if( tImporter != null ) {
            tImporter.isReadable = true;
			tImporter.textureFormat = TextureImporterFormat.ARGB32;
            AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
			UnityEngine.Object t = AssetDatabase.LoadAssetAtPath( path, typeof( Texture2D ) );
        	if( t != null ) {
            	return ( (Texture2D)t ).EncodeToPNG();
        	}
			tImporter.textureFormat = TextureImporterFormat.AutomaticCompressed;
        }
		
		// Return a byte array that has a value of '0' if the conversion failed.
		byte[] empty = BitConverter.GetBytes(0);
		return empty;
	}
	
	static Vector3[] GetColliderVertexPositions(GameObject obj) {
	    Vector3[] vertices = new Vector3[8];
	    Matrix4x4 thisMatrix = obj.transform.localToWorldMatrix;
	    Quaternion storedRotation = obj.transform.rotation;
	    obj.transform.rotation = Quaternion.identity;
	
	    Vector3 extents = obj.collider.bounds.extents;
	    vertices[0] = thisMatrix.MultiplyPoint3x4(extents);
	    vertices[1] = thisMatrix.MultiplyPoint3x4(new Vector3(-extents.x, extents.y, extents.z));
	    vertices[2] = thisMatrix.MultiplyPoint3x4(new Vector3(extents.x, extents.y, -extents.z));
	    vertices[3] = thisMatrix.MultiplyPoint3x4(new Vector3(-extents.x, extents.y, -extents.z));
	    vertices[4] = thisMatrix.MultiplyPoint3x4(new Vector3(extents.x, -extents.y, extents.z));
	    vertices[5] = thisMatrix.MultiplyPoint3x4(new Vector3(-extents.x, -extents.y, extents.z));
	    vertices[6] = thisMatrix.MultiplyPoint3x4(new Vector3(extents.x, -extents.y, -extents.z));
	    vertices[7] = thisMatrix.MultiplyPoint3x4(-extents);
	    obj.transform.rotation = storedRotation;
	
	    return vertices;
	}
}

