using UnityEngine;
using UnityEditor;
using System.Collections;

public class Player : MonoBehaviour
{
	public string levelName = "";
	
	void OnDrawGizmos()
	{
		Gizmos.DrawIcon(gameObject.transform.position, "PlayerStart.png");
	}
}

public class CreatePlayer : EditorWindow
{   
	static Camera cam;
	
	[MenuItem("Shuriken/Create Player Start", false, 0)]
    static void Init()
    {
		cam = Camera.current;
		
		GameObject player = new GameObject();
		player.AddComponent<Player>();
		if (cam) {
			player.transform.position = cam.transform.position + cam.transform.forward*5.0f;
		}
		player.tag = "Player";
		player.name = "Player Start";
		Selection.activeObject = player;
	}
	
    [MenuItem("Shuriken/Create Player Start", true)]
	static bool Validate() {
		GameObject[] objects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];
      	foreach(GameObject gameObj in objects) {
			// If we find terrain, export it with the terrain exporter thingy
		    if(gameObj.tag == "Player") {
				//EditorUtility.DisplayDialog("Ninja Stylin'", "There is already a player start in the level. There can only be one!", "Oh!");
				return false;
			}
		}
		
		return true;
	}
}