using UnityEngine;
using UnityEditor;
using System.Collections;

public class Fire : MonoBehaviour
{
	public void SetEmitter(ParticleEmitter a_emitter)
	{
		emitter = a_emitter;
	}
	
	public float fireHeight = 1;
	public float fireSize = 5;
	public float emissionRate = 50;
	public float lifeTime = 1;
	private ParticleEmitter emitter;
	
	void OnDrawGizmos()
	{
		//emitter.worldVelocity;
		emitter.particleEmitter.worldVelocity = new Vector3(0,fireHeight,0);
		emitter.particleEmitter.rndVelocity = new Vector3(1,fireHeight/2,1);
		emitter.minEmission = emissionRate;
		emitter.maxEmission = emissionRate;
		emitter.maxSize = fireSize/5;
		emitter.minSize = fireSize/10;
		emitter.minEnergy = lifeTime/2;
		emitter.maxEnergy = lifeTime;
	}
}

public class CreateFire : EditorWindow
{   
	static Camera cam;
	
	[MenuItem("Shuriken/Create Fire", false, 1)]
    static void Init()
    {
		cam = Camera.current;
		
		GameObject fire = (GameObject)Instantiate(Resources.Load("Fire"));
		fire.AddComponent<Fire>();
		fire.GetComponent<Fire>().SetEmitter(fire.GetComponentInChildren<ParticleEmitter>());
		if (cam) {
			fire.transform.position = cam.transform.position + cam.transform.forward*5.0f;
		}
		fire.tag = "Fire";
		fire.name = "Fire";
		Selection.activeObject = fire;
	}
}