using UnityEngine;
using UnityEditor;
using System.Collections;

public class Guard : MonoBehaviour
{
	public PathNode startNode;
	
	void OnDrawGizmos()
	{
		Gizmos.DrawIcon(gameObject.transform.position, "Guard.png");
		Gizmos.color = Color.gray;
		if (startNode) Gizmos.DrawLine(gameObject.transform.position,startNode.transform.position);
	}
}

public class CreateGuard : EditorWindow
{   
	static Camera cam;
	
	[MenuItem("Shuriken/Create Guard", false, 1)]
    static void Init()
    {
		cam = Camera.current;
		
		GameObject guard = new GameObject();
		guard.AddComponent<Guard>();
		if (cam) {
			guard.transform.position = cam.transform.position + cam.transform.forward*5.0f;
		}
		guard.tag = "Guard";
		guard.name = "Guard";
		Selection.activeObject = guard;
	}
}